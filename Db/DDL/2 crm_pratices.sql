
CREATE TABLE `crm_practice` (
  `id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `tech_notes` text,
  `practical_notes` text,
  `cost` float NOT NULL,
  `accessories` int(11) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `serial` varchar(250) DEFAULT NULL,
  `id_customer` bigint(20) NOT NULL,
  `id_typeop` bigint(20) NOT NULL,
  `id_typeopproduct` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `crm_practice`
--
ALTER TABLE `crm_practice`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `crm_practice`
--
ALTER TABLE `crm_practice`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;



CREATE TABLE `crm_pratice_operation` (
  `id` bigint(20) DEFAULT NULL COMMENT 'Campo usato solo per Eloquent',
  `id_pratice` bigint(20) NOT NULL,
  `id_operation` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `crm_pratice_operation`
--
ALTER TABLE `crm_pratice_operation`
  ADD PRIMARY KEY (`id_pratice`,`id_operation`);
COMMIT;