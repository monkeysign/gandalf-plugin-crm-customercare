
CREATE TABLE `crm_typeoperation` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `crm_typeoperation`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `crm_typeoperation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;




CREATE TABLE `crm_typeopproduct` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `id_typeop` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `crm_typeopproduct`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `crm_typeopproduct`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;



CREATE TABLE `crm_operation` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `id_typeopproduct` bigint(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `crm_operation`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `crm_operation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;