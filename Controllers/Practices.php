<?php

namespace Plugins\CRM\CustomerCare\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CRM\Customer\Models\Customer as Customer;
use Plugins\CRM\CustomerCare\Models\Operation;
use Plugins\CRM\CustomerCare\Models\Practice as Practice;
use Plugins\CRM\CustomerCare\Models\PraticeOperation;
use Plugins\CRM\CustomerCare\Models\TypeOp;
use Plugins\CRM\CustomerCare\Models\TypeOpProduct;
use setasign\Fpdi\Fpdi;

class Practices extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        $this->param['table'] = Practice::where('archived', 0)->orderBy('id', 'DESC')->get();
        return view()->render('crm-customercare.practice.list', $this->param);
    }

    /**
     * Lista di tutti i record
     */
    public function listAllArchived() {
        $this->param['table'] = Practice::where('archived', 1)->orderBy('id', 'DESC')->get();
        return view()->render('crm-customercare.practice.listarchived', $this->param);
    }

    /**
     * Ajax Prodotti
     */
    public function ajaxProducts() {
        return TypeOpProduct::where('id_typeop', request()->get('idTypeOp'))->get();
    }

    /**
     * Ajax Operations
     */
    public function ajaxOperations() {
        return Operation::where('id_typeopproduct', request()->get('idTypeOpProduct'))->get();
    }

    /**
     * Genero il json per la lista
     * @return array
     * @throws \Exception
     */
    public function recordList() {
        $record = new Practice();
        $dataTable = hooks()->apply_filters(CRM_ADMIN_CUSTOMERCARE_PRACTICE_LIST, $record);
        return $dataTable->make();
    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form($id = null) {
        $param['allCustomer'] = Customer::orderBy('name', 'ASC')->get();
        $param['allTypeOp'] = TypeOp::orderBy('title', 'ASC')->get();

        if (isset($id) && $id) {
            $param['record'] = Practice::find($id);
            $param['allOp'] = TypeOpProduct::where('id_typeop', $param['record']->id_typeop)->orderBy('title', 'ASC')->get();
            $param['allInterventi'] = Operation::where('id_typeopproduct', $param['record']->id_typeopproduct)->orderBy('title', 'ASC')->get();
        } else {
            $param['record'] = new Practice();
        }


        return view()->render('crm-customercare.practice.form', $param);
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get('item');
        try {
            //saveOrUpdate la Pratica
            $record = Practice::saveOrUpdate($item);
            $operations = request()->get('operations');
            $record->operations()->detach();
            if ($operations) {
                foreach ($operations as $key => $op) {
                    if (!is_numeric($op)) {
                        $newOptionOp = explode("-", $op);
                        $label = $newOptionOp[0];
                        $price = 0;
                        if ($newOptionOp[1])
                            $price = $newOptionOp[1];
                        $itemOperation = array('title' => $label, 'price' => $price, 'id_typeopproduct' => $record->id_typeopproduct);
                        $recordOperation = Operation::saveOrUpdate($itemOperation);
                        $operations[$key] = $recordOperation->id;
                    }
                }
                $record->operations()->attach($operations);
            }
            $param = [
                'record' => $record,
                'state' => true,
                'mex' => 'Salvataggio Riuscito'
            ];
        } catch (\Ring\Exception\ValidationException $ex) {
            die($ex->getMessage());
        }

        return $param;
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete($id = null) {
        $record = Practice::find($id);
        $record->operations()->detach();
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array('result' => true);

        return $data;
    }

    /**
     * @param null $id
     * @return array
     */
    public function closePractice($id = null) {

        $record = Practice::find($id);
        $record->status = 3;
        $record->archived = 1;
        $record->cost = (floatval(request()->query('cost')) > 0) ? floatval(request()->query('cost')) : $record->cost;
        $record->save();
//        return redirector()->route('admin.crm.customercare.practice.list');
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array('result' => true, 'cost' => $record->cost, 'req' =>request()->query('cost'));
        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get('ids');
        Practice::whereIn('id', $group)->delete();
        $data = array('result' => true);

        return $data;
    }


    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function sendsms($id = null) {
        $record = Practice::find($id);
        $numTel = $record->customer->meta('phone');
        $error = '';
        if (strlen($numTel) > 1) {
            if (strcmp("+", substr(strtolower(trim($numTel)), 0, 1)) != 0) {
                $numTel = "+39" . $numTel;
            }
            $prodotto = $record->typeOpProduct;
            if ($prodotto && strlen($prodotto->title) > 0) {
                $messaggio = "Gentile cliente, siamo lieti di comunicarti che il tuo dispositivo $prodotto->title è pronto per essere ritirato presso il nostro store. Saluti, blobvideo.com";
                //mess max 160 caratteri
                if (strlen($messaggio) > 160)
                    $messaggio = substr($messaggio, 0, 160);
                $paramSms = array('user' => 'blobvideo', 'pass' => 'smspepe', 'rcpt' => $numTel, 'data' => $messaggio, 'sender' => '+390815152511', 'qty' => 'n');
            } else {
                $error = 'Errore invio notifica sms: errore nel recupero del prodotto';
            }
        } else {
            $error = 'Errore invio notifica sms: numero non valido';
        }
        if (strlen($error) == 0) {
            $responseSms = $this->sendSmsMobile($paramSms);
            if (strcmp("ok", substr(strtolower(trim($responseSms)), 0, 2)) == 0) {
                $data = array('result' => true, 'responseSms' => $responseSms);
                // salvo l'invio dell'sms
                $record->sms_sended = 1;
                $record->save();
            } else {
                $data = array('result' => false, 'responseSms' => $responseSms);
            }
        } else {
            $data = array('result' => false, 'responseSms' => $error);
        }
        return $data;
    }


    public function printPractice($id = null) {
        $practice = Practice::find($id);
        $pdf = new Fpdi();
        $pdf->AddPage();

        $pdf->Image(realpath($_SERVER['DOCUMENT_ROOT']) . '/themes/' . theme() . '/assets/assist.jpg', 0, 0, 210, 297);

        $pdf->SetFont('Arial', 'B', 9);
        $gap = 0;

        for ($i = 0; $i <= 1; $i++) {
            $pdf->Text(28.5, 31 + $gap, $practice->id);
            $pdf->Text(61, 31 + $gap, date('d/m/Y', strtotime($practice->created_at)));
            $pdf->Text(105, 30.5 + $gap, $practice->customer->name);
            $pdf->Text(165, 30.5 + $gap, $practice->customer->surname);

            $pdf->Text(17, 35.8 + $gap, $practice->customer->meta('phone'));
            $pdf->Text(63, 35.8 + $gap, $practice->customer->email);

            $pdf->Text(30.5, 45.5 + $gap, $practice->typeOpProduct->title);
            $pdf->Text(32, 50.5 + $gap, $practice->serial);
            $pdf->Text(31, 55.5 + $gap, ($practice->quotation) ? 'Si' : 'No');

            $allOperations = $practice->operations;
            $allTitle = '';
            foreach ($allOperations as $op) {
                $allTitle .= $op->title . ', ';
            }
            $allTitle = substr($allTitle, 0, -2);

            $pdf->Text(127, 45.5 + $gap, $allTitle);
            $pdf->Text(131, 50.5 + $gap, $practice->accessories);
            $pdf->Text(121, 55.5 + $gap, number_format($practice->cost, 2, ',', '.'));

            $pdf->Text(20.5, 60.5 + $gap, $practice->practical_notes);

            $gap = 145.5;
        }
        $pdf->Output('pratica-' . $id . '.pdf', 'D');

    }

    public function sendSmsMobile($fields, $host = 'sms.smsmobile-ba.com', $url = '/sms/send.php') {
        $qs = array();
        foreach ($fields as $k => $v)
            $qs[] = $k . '=' . urlencode($v);
        $qs = join('&', $qs);
        $errno = $errstr = '';
        if ($fp = @fsockopen('sms.smsmobile-ba.com', 80, $errno, $errstr, 30)) {
            fputs($fp, "POST " . $url . " HTTP/1.0\r\n");
            fputs($fp, "Host: " . $host . "\r\n");
            fputs($fp, "User-Agent: PHP/" . phpversion() . "\r\n");
            fputs($fp, "Content-Type:application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-Length: " . strlen($qs) . "\r\n");
            fputs($fp, "Connection: close\r\n");
            fputs($fp, "\r\n" . $qs);
            $content = '';
            while (!feof($fp))
                $content .= fgets($fp, 1024);
            fclose($fp);
            return preg_replace("/^.*?\r\n\r\n/s", '', $content);
        }
        return $errstr;
    }


}