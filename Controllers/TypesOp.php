<?php
namespace Plugins\CRM\CustomerCare\Controllers;

use Modules\Backend\Classes\Controller;
use Plugins\CRM\CustomerCare\Models\TypeOp;


class TypesOp extends Controller {

    /**
     * Lista di tutti i record
     */
    public function listAll() {
        return view()->render( 'crm-customercare.typeop.list', [] );
    }

    /**
     * Genero il json per la lista
     * @return array
     * @throws \Exception
     */
    public function recordList() {
        $record    = new TypeOp();
        $dataTable = hooks()->apply_filters( CRM_ADMIN_CUSTOMERCARE_TYPEOP_LIST, $record );
        return $dataTable->make();
    }

    /**
     * Azione di form insert/update
     *
     * @param null $id
     */
    public function form( $id = null ) {
        if ( isset( $id ) && $id ) {
            $param['record'] = TypeOp::find( $id );
        } else {
            $param['record'] = new TypeOp();
        }

        return view()->render( 'crm-customercare.typeop.form', $param );
    }

    /**
     * Azione di salvataggio
     * @return array
     */
    public function save() {
        $item = request()->get( 'item' );
        try {
            $record = TypeOp::saveOrUpdate( $item );
            $param = [
                'record' => $record,
                'state'  => true,
                'mex'    => 'Salvataggio Riuscito'
            ];
        } catch ( \Ring\Exception\ValidationException $ex ) {
            die( $ex->getMessage() );
        }

        return $param;
    }

    /**
     * Delete di un record e meta associati
     *
     * @param null $id
     *
     * @return array
     */
    public function delete( $id = null ) {
        $record = TypeOp::find( $id );
        $record->delete();
        //usare forceDelete() solo se si vuole una cancellazione fisica
        $data = array( 'result' => true );

        return $data;
    }

    /**
     * Delete di un gruppo e meta associati
     * @return array
     */
    public function deleteGroup() {
        // $_POST['ids']
        $group = request()->get( 'ids' );
        TypeOp::whereIn( 'id', $group )->delete();
        $data = array( 'result' => true );

        return $data;
    }



}