<?php

const _P_CRM_CUSTOMERCARE = "\\Plugins\\CRM\\CustomerCare\\Controllers\\";
// Admin group
router()->group(['middleware' => ['web']], function () {
    //auth route
    router()->group(['middleware' => ['auth'], 'prefix' => 'admin/crm'], function () {
        //customarecare
        router()->group(['prefix' => 'customercare'], function () {
            //router()->get('', ['as' => 'admin.crm.customercare.base', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@listAll']);

            //TypeOp
            router()->group(['prefix' => 'typeop'], function () {
                router()->get('', [ 'as' => 'admin.crm.customercare.typeop.base', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@listAll' ]);
                router()->get('/list', [ 'as' => 'admin.crm.customercare.typeop.list', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@listAll' ]);
                router()->post('/list/recordList', ['as' => 'admin.crm.customercare.typeop.list.recordList', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@recordList']);
                router()->get('/add', ['as' => 'admin.crm.customercare.typeop.add', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@form']);
                router()->get('/{id}', ['as' => 'admin.crm.customercare.typeop.update', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@form']);
                router()->post('/save', ['as' => 'admin.crm.customercare.typeop.save', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@save']);
                router()->get('/delete/{id}', ['as' => 'admin.crm.customercare.typeop.delete', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOp@delete']);
                router()->post('/delete', [ 'as' => 'admin.crm.customercare.typeop.deletegroup', 'uses' => _P_CRM_CUSTOMER . 'TypesOp@deleteGroup' ]);
            });


            /**
             * TYPEPRODUCT
             */
            router()->group(['prefix' => 'typeopproduct'], function () {
                router()->get('/list', ['as' => 'admin.crm.customercare.typeopproduct.list', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@listAll']);
                router()->post('/list/recordList', ['as' => 'admin.crm.customercare.typeopproduct.list.recordList', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@recordList']);
                router()->get('/add', ['as' => 'admin.crm.customercare.typeopproduct.add', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@form']);
                router()->get('/{id}', ['as' => 'admin.crm.customercare.typeopproduct.update', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@form']);
                router()->post('/save', ['as' => 'admin.crm.customercare.typeopproduct.save', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@save']);
                router()->get('/delete/{id}', ['as' => 'admin.crm.customercare.typeopproduct.delete', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@delete']);
                router()->post('/delete', ['as' => 'admin.crm.customercare.typeopproduct.deletegroup', 'uses' => _P_CRM_CUSTOMERCARE . 'TypesOpProducts@deleteGroup']);
            });


            /**
             * OPERATION
             */
            router()->group(['prefix' => 'operation'], function () {
                router()->get('/list', ['as' => 'admin.crm.customercare.operation.list', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@listAll']);
                router()->post('/list/recordList', ['as' => 'admin.crm.customercare.operation.list.recordList', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@recordList']);
                router()->get('/add', ['as' => 'admin.crm.customercare.operation.add', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@form']);
                router()->get('/{id}', ['as' => 'admin.crm.customercare.operation.update', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@form']);
                router()->post('/save', ['as' => 'admin.crm.customercare.operation.save', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@save']);
                router()->get('/delete/{id}', ['as' => 'admin.crm.customercare.operation.delete', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@delete']);
                router()->post('/delete', ['as' => 'admin.crm.customercare.operation.deletegroup', 'uses' => _P_CRM_CUSTOMERCARE . 'Operations@deleteGroup']);
            });

	        /**
	         * PRATICHE
	         */
	        router()->group(['prefix' => 'practice'], function () {
		        router()->get('/list', ['as' => 'admin.crm.customercare.practice.list', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@listAll']);
                router()->get('/listarchived', ['as' => 'admin.crm.customercare.practice.listarchived', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@listAllArchived']);
		        router()->post('/list/recordList', ['as' => 'admin.crm.customercare.practice.list.recordList', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@recordList']);
		        router()->post('/ajax/products', ['as' => 'admin.crm.customercare.practice.ajax.products', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@ajaxProducts']);
		        router()->post('/ajax/operations', ['as' => 'admin.crm.customercare.practice.ajax.operations', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@ajaxOperations']);
		        router()->get('/add', ['as' => 'admin.crm.customercare.practice.add', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@form']);
		        router()->get('/{id}', ['as' => 'admin.crm.customercare.practice.update', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@form']);
		        router()->post('/save', ['as' => 'admin.crm.customercare.practice.save', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@save']);
		        router()->get('/delete/{id}', ['as' => 'admin.crm.customercare.practice.delete', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@delete']);
		        router()->post('/delete', ['as' => 'admin.crm.customercare.practice.deletegroup', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@deleteGroup']);
                router()->get('/sendsms/{id}', ['as' => 'admin.crm.customercare.practice.sendsms', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@sendsms']);
                router()->get('/print/{id}', ['as' => 'admin.crm.customercare.practice.print', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@printPractice']);
                router()->get('/close/{id}', ['as' => 'admin.crm.customercare.practice.close', 'uses' => _P_CRM_CUSTOMERCARE . 'Practices@closePractice']);
	        });
        });
    });
});