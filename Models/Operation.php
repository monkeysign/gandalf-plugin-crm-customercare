<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 19/05/2018
 * Time: 08:14
 */

namespace Plugins\CRM\CustomerCare\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;


class Operation extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'crm_operation';
    protected $fillable = array('title', 'price', 'id_typeopproduct');

    public function product() {
        return $this->belongsTo('Plugins\CRM\CustomerCare\Models\TypeOpProduct', 'id', 'id_typeopproduct');
    }


    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_OPERATION_SAVE, $item );
            return Operation::create($item);
        } else {
            Operation::where('id', '=', $item['id'])->update($item);
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_OPERATION_SAVE, $item );
            return Operation::find($item['id']);
        }
    }
}