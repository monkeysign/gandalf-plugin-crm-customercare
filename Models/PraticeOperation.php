<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 22/06/2018
 * Time: 22:19
 */

namespace Plugins\CRM\CustomerCare\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of User
 *
 * @author Riccardo
 */
class PraticeOperation extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'crm_pratice_operation';
    protected $fillable = array('id_operation', 'id_pratice');

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            //hooks()->do_action( CRM_CUSTOMERCARE_PRACTICE_SAVE, $item );
            return PraticeOperation::create($item);
        } else {
            PraticeOperation::where('id', '=', $item['id'])->update($item);
            //hooks()->do_action( CRM_CUSTOMERCARE_PRACTICE_SAVE, $item );
            return PraticeOperation::find($item['id']);
        }
    }
}