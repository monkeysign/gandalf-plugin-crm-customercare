<?php


namespace Plugins\CRM\CustomerCare\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of User
 *
 * @author Riccardo
 */
class TypeOp extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'crm_typeoperation';
    protected $fillable = array('title');

    public function products() {
        return $this->hasMany('Plugins\CRM\CustomerCare\Models\TypeOpProduct', 'id_typeop', 'id');
    }


    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_TYPEOP_SAVE, $item );
            return TypeOp::create($item);
        } else {
            TypeOp::where('id', '=', $item['id'])->update($item);
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_TYPEOP_SAVE, $item );
            return TypeOp::find($item['id']);
        }
    }
}