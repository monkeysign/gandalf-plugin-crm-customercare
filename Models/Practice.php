<?php

namespace Plugins\CRM\CustomerCare\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of User
 *
 * @author Riccardo
 */
class Practice extends Eloquent {
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table = 'crm_practice';
	protected $fillable = array('id_customer', 'status', 'tech_notes','practical_notes','cost' ,'accessories' ,'type' , 'quotation', 'date','serial' ,'id_customer' ,'id_typeop' ,'id_typeopproduct', 'unlock_code', 'express_repair', 'sms_sended', 'archived');

    /**
     * Get Interventi
     */
    public function operations() {
        return $this->belongsToMany( 'Plugins\CRM\CustomerCare\Models\Operation', 'crm_pratice_operation', 'id_pratice', 'id_operation' );
    }

    public function customer() {
        return $this->hasOne( 'Plugins\CRM\Customer\Models\Customer', 'id', 'id_customer' );
    }

    public function typeOpProduct() {
        return $this->hasOne( 'Plugins\CRM\CustomerCare\Models\TypeOpProduct', 'id', 'id_typeopproduct' );
    }

    public function typeOp() {
        return $this->hasOne( 'Plugins\CRM\CustomerCare\Models\TypeOp', 'id', 'id_typeop' );
    }

    public function getStatusLabel() {
        switch($this->status){
            case 1:
                return 'In Lavorazione';
                break;
            case 2:
                return 'Attesa Accettazione Preventivo';
                break;
            case 3:
                return 'Completata';
                break;
            case 4:
                return 'Non Riparabile';
                break;
            default:
                return '-';
                break;
        }
    }

	public static function saveOrUpdate($item) {
		if (!isset($item['id']) || !$item['id']) {
			//hooks()->do_action( CRM_CUSTOMERCARE_PRACTICE_SAVE, $item );
			return Practice::create($item);
		} else {
			Practice::where('id', '=', $item['id'])->update($item);
			//hooks()->do_action( CRM_CUSTOMERCARE_PRACTICE_SAVE, $item );
			return Practice::find($item['id']);
		}
	}
}