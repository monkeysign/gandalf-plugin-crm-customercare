<?php

namespace Plugins\CRM\CustomerCare\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeOpProduct extends Eloquent {
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'crm_typeopproduct';
    protected $fillable = array('title', 'id_typeop');

    public function typeop() {
        return $this->belongsTo('Plugins\CRM\CustomerCare\Models\TypeOp', 'id', 'id_typeop');
    }

   public function operation() {
        return $this->hasMany('Plugins\CRM\CustomerCare\Models\Operation', 'id_typeopproduct', 'id');
    }

    public static function saveOrUpdate($item) {
        if (!isset($item['id']) || !$item['id']) {
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_TYPEOPPRODUCT_SAVE, $item );
            return TypeOpProduct::create($item);
        } else {
            TypeOpProduct::where('id', '=', $item['id'])->update($item);
            hooks()->do_action( CRM_ADMIN_CUSTOMERCARE_TYPEOPPRODUCT_SAVE, $item );
            return TypeOpProduct::find($item['id']);
        }
    }
}

