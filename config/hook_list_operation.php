<?php
/**
 * Created by PhpStorm.
 * User: fofy
 * Date: 19/05/2018
 * Time: 08:37
 */

/**
 * Aggiungo array dei campi da prendere per ogni colonna
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST, function ( $record ) {
    //$record->with('crm_typeopproduct');
    $fields = [
        'id',
        'title',
        'id_typeopproduct',
        'price'
    ];

    return [ $fields, $record ];
}, 10 );

/**
 * Hook per le colonne html della tabella
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST_COLUMN, function ( $col = [] ) {
    $columns[0] = '<th><input type="checkbox" class="selectallrow"></th>';
    $columns[1] = '<th>Titolo</th>';
    $columns[2] = '<th class="no-filter">Prodotto</th>';
    $columns[3] = '<th>Prezzo</th>';
    $columns[4] = '<th class="no-filter">Azioni</th>';

    return $columns;
}, 1 );

hooks()->add_filter( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST_COLUMN, function ( $columns ) {
    foreach ( $columns as $col ) {
        echo $col;
    }
}, 100 );

/**
 * Hook dei filtri per la visualizzazione delle colonne
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST_FILTER, function ( $record ) {

    $formatting[0] = function ( $record ) {
        return '<input type="checkbox" class="selectrow" value="' . $record->id . '">';
    };

    $formatting[1] = function ( $record ) {
        return $record->title;
    };

    $formatting[2] = function ( $record ) {
        $prodotto = \Plugins\CRM\CustomerCare\Models\TypeOpProduct::where('id', $record->id_typeopproduct)->first();
        if ( $prodotto ) {
            return $prodotto->title;
        }
        return "prootto non disponibile";
    };

    $formatting[3] = function ( $record ) {
        return $record->price;
    };

    $formatting[4] = function ( $record ) {
        return '
<div class="btn-group" role="group">
	<a href="' . path_for( 'admin.crm.customercare.operation.update', [ 'id' => $record->id ] ) . '" class="btn btn-sm btn-default">
		<span class="fa fa-pencil"></span>
	</a>
    <a data-path="' . path_for( 'admin.crm.customercare.operation.delete', [ 'id' => $record->id ] ) . '" class="btn btn-sm btn-danger delete-item">
        <span class="fa fa-trash-o"></span>
    </a>
</div>
';
    };

    return [ $record, $formatting ];
}, 1 );

/**
 * Effettuo la creazione della tabella applicando i filtri alle colonne
 */
hooks()->add_filter( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST, function ( $params ) {

    list( $fields, $record ) = $params;
    // do la struttura del dataTable
    $dataTable = new \LiveControl\EloquentDataTable\DataTable( $record->orderByDesc( 'id' ), $fields );

    // Formatto le colonne
    $dataTable->setFormatRowFunction( function ( $record ) {
        list( $record, $formatting ) = hooks()->apply_filters( CRM_ADMIN_CUSTOMERCARE_OPERATION_LIST_FILTER, $record );
        foreach ( $formatting as $format ) {
            $fieldFormat[] = $format( $record );
        }

        return $fieldFormat;
    } );

    // torno l'oggetto
    return $dataTable;
}, 100 );
