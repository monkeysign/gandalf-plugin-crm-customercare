<?php
return [
	"name"   => 'CRM-CustomerCare',
	"class"  => \Plugins\CRM\CustomerCare\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];