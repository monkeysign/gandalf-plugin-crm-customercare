@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('crm-customercare.practice.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>Visualizzazione Pratica</strong>
                            @else
                                Nuova Pratica
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.crm.customercare.practice.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <input type="hidden" id="toPrint" name="toPrint" value="">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Seleziona un cliente"
                                                    class="control-label">Cliente</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select @if($record->id){{"disabled"}} @endif name="item[id_customer]" class="form-control select-choose"
                                                        placeholder="Seleziona un cliente">
                                                    @foreach($allCustomer as $customer)
                                                        <option @if($customer->id == $record->id_customer){{"selected"}}@endif value="{{$customer->id}}">{{$customer->name}} {{$customer->surname}}
                                                            - {{$customer->meta('phone')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Seleziona un cliente"
                                                    class="control-label">Tipologia Pratica</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select @if($record->id){{"disabled"}} @endif id="typeop" name="item[id_typeop]"
                                                        class="form-control select-choose"
                                                        placeholder="Seleziona un cliente">
                                                    <option @if(!$record->id){{"selected"}} @endif>Seleziona una
                                                        tipologia
                                                    </option>
                                                    @foreach($allTypeOp as $typeOp)
                                                        <option @if($typeOp->id == $record->id_typeop){{"selected"}}@endif value="{{$typeOp->id}}">{{$typeOp->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                    title="Seleziona un cliente"
                                                    class="control-label">Prodotto</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select id="id_typeopproduct" disabled name="item[id_typeopproduct]" class="form-control select-choose"
                                                        placeholder="Seleziona un cliente">
                                                    <option @if(!$record->id_typeopproduct){{"selected"}}@endif>Seleziona un prodotto</option>
                                                    @foreach($allOp as $Op)
                                                        <option @if($Op->id == $record->id_typeopproduct){{"selected"}}@endif value="{{$Op->id}}">{{$Op->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                </div>
                                <!--/row-->
                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label
                                                    title="Seleziona un cliente"
                                                    class="control-label">Interventi</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select @if(!$record->id_typeopproduct) {{"disabled"}} @endif id="operations" multiple
                                                        class="form-control select-choose"
                                                        placeholder="Seleziona un intervento">
                                                    @foreach($allInterventi as $Intervento)
                                                        <option value="{{$Intervento->id}}" data-price ="{{$Intervento->price}}">{{$Intervento->title}} - {{$Intervento->price}} Eur</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>Elenco interventi effettuati</h4>
                                        <table class="table table-bordered table-hover table-striped">
                                            <tbody id="divop">

                                            @if ( $record->operations())
                                                @foreach($record->operations as $intervento)
                                                    <tr>
                                                        <td class="pt-2 pb-2">
                                                            <strong> {{$intervento->title}} ({{ number_format($intervento->price, 2, '.', ',') }} Eur)</strong>
                                                            <input type="hidden" name="operations[]" value="{{$intervento->id}}">
                                                            <i onclick="javascript:cancellaItem($(this),{{$intervento->price}})" class="fa fa-remove button-iterventi"></i>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Pratica"
                                                            class="control-label">Stato Pratica</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[status]" class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->status == 1)
                                                                    selected @endif
                                                                    value="1">In Lavorazione
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 2)
                                                                    selected @endif
                                                                    value="2">Attesa Accettazione Preventivo
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 3)
                                                                    selected @endif
                                                                    value="3">Completata
                                                            </option>
                                                            <option
                                                                    @if ( $record->status == 4)
                                                                    selected @endif
                                                                    value="4">Non Riparabile
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label
                                                            title="Preventivo"
                                                            class="control-label">Preventivo</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[quotation]"
                                                                class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->quotation == 0)
                                                                    selected @endif
                                                                    value="0">No
                                                            </option>
                                                            <option
                                                                    @if ( $record->quotation == 1)
                                                                    selected @endif
                                                                    value="1">Si
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label
                                                            title="Espressa"
                                                            class="control-label">Espressa</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <select name="item[express_repair]"
                                                                class="form-control select-choose">
                                                            <option
                                                                    @if ( $record->express_repair == 1)
                                                                    selected @endif
                                                                    value="1">Si
                                                            </option>
                                                            <option
                                                                    @if ( $record->express_repair == 0)
                                                                    selected @endif
                                                                    value="0">No
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Pratica"
                                                            class="control-label">IMEI/Seriale</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <input type="text" name="item[serial]" class="form-control" placeholder="IMEI/Seriale" value="{{ $record->serial }}">
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Pratica"
                                                            class="control-label">Accessori in dotazione</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <input type="text" name="item[accessories]" class="form-control" placeholder="Accessori" value="{{ $record->accessories }}">
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                            title="Stato Pratica"
                                                            class="control-label">Codice di Sblocco</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="ti-light-bulb"></i>
                                                        </div>
                                                        <input type="text" name="item[unlock_code]" class="form-control" placeholder="Codice di Sblocco" value="{{ $record->unlock_code }}">
                                                    </div>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label
                                                        title="Stato Pratica"
                                                        class="control-label">Note pratica</label>
                                                <textarea style="height: 250px;" name="item[practical_notes]" class="form-control">{{$record->practical_notes}}</textarea>
                                                <span class="help-block"> </span>
                                            </div>
                                            <div class="col-md-6">
                                                <label
                                                        title="Stato Pratica"
                                                        class="control-label">Note tecniche</label>
                                                <textarea style="height: 250px;" name="item[tech_notes]" class="form-control">{{$record->tech_notes}}</textarea>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- hooks()->do_action(CRM_ADMIN_CUSTOMER_FORM, $record) --}}

                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div><strong>Costo totale chiusura pratica:</strong></div>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="ti-money"></i>
                                            </div>
                                            <input id="cost" type="text" name="item[cost]" class="form-control" placeholder="Totale" value="{{ number_format($record->cost, 2, '.', ',') }}">
                                        </div>
                                        <span class="help-block"> </span>
                                    </div>
                                    <div class="col-md-3">
                                        <div>&nbsp;</div>
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-save"></i>
                                            Salva
                                        </button>
                                        <a id="printAndSave" class="btn btn-info">
                                            <i class="fa fa-print"></i>
                                            Salva e Stampa
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        // Select Choosen
        $(function () {
            $(".select-choose").select2();

            var $typeop = $('#typeop').select2();
            var $idtypeopproduct = $('#id_typeopproduct').select2();
            var $operations = $('#operations').select2({
                tags: true
            });

            $('#printAndSave').click(function (e) {
                $('#toPrint').val('ok');
                $('#form-save').submit();
            })

            $typeop.on('select2:select', function (e) {
                $.ajax({
                    method: "POST",
                    url: '{{ path_for('admin.crm.customercare.practice.ajax.products') }}',
                    data: {idTypeOp: this.value},
                    dataType: "json",
                    success: function (data) {
                        var newOption, firstselected = '';
                        $idtypeopproduct.empty().trigger("change");
                        data.map(function (record, index) {
                            if (index === 0) firstselected = record.id;
                            newOption = new Option(record.title, record.id, false, false);
                            $idtypeopproduct.append(newOption);
                        })
                        if(firstselected==''){
                            $idtypeopproduct.prop("disabled", true);
                            $idtypeopproduct.empty().trigger("change");
                            $operations.prop("disabled", true);
                            $operations.empty().trigger("change");
                        }else{
                            $idtypeopproduct.removeAttr('disabled').val("" + firstselected).trigger('select2:select')
                        }
                    }
                })

                $('#divop').html("");
            });


            $idtypeopproduct.on('select2:select', function (e) {
                if(this.value>0){ // se ho prodotti per quella tipologia
                    $.ajax({
                        method: "POST",
                        url: '{{ path_for('admin.crm.customercare.practice.ajax.operations') }}',
                        data: {idTypeOpProduct: this.value},
                        dataType: "json",
                        success: function (data) {
                            var newOption;
                            $operations.empty().trigger("change");
                            data.map(function (record, index) {
                                if(record.price == undefined || record.price == '') record.price = 0;
                                newOption = new Option(record.title+ " - "+parseFloat(record.price).toFixed(2)+ " Eur", record.id, false, false);
                                newOption.dataset.price = record.price;
                                $operations.append(newOption);
                            })
                            $operations.removeAttr('disabled').trigger('change');
                        }
                    })
                }else{ // se non ho prodotti disabilito la select degli interventi
                     $operations.prop("disabled", true);
                    $operations.empty().trigger("change");
                }
                $('#divop').html("");
            });


           $operations.on('select2:select', function (e) {
               var data = e.params.data;
               var label = data.text.split("-")[0];
               var prezzo = 0;

               if(isNaN(parseFloat($('#cost').val()))){
                   $('#cost').val(0);
               }


               if(data.element === undefined){
                   prezzo = parseFloat((data.text.split("-")[1]).replace(",", "."));
               } else {
                   prezzo = parseFloat(data.element.dataset.price);
               }

               $('#cost').val( (parseFloat($('#cost').val())+ prezzo).toFixed(2));

               $('#divop').append(
                   '<tr>' +
                   '<td class="pt-2 pb-2">' +
                   '<strong>' + label + ' ('+prezzo.toFixed(2)+' Eur)</strong>' +
                   '<input type="hidden" name="operations[]" value="' + data.id + '">' +
                   '<i onclick="javascript:cancellaItem($(this), ' + prezzo + ')" class="fa fa-remove button-iterventi"></i>' +
                   '</td>' +
                   '</tr>');
               $operations.val(null).trigger("change");
           })

        });


        function cancellaItem(elem,prezzo){
            decrementaCosto(prezzo);
            elem.parent().parent().remove();
        }

        function decrementaCosto(prezzo){
            if(isNaN(parseFloat($('#cost').val()))){
                $('#cost').val(0);
            }
            console.log(parseFloat($('#cost').val()-prezzo).toFixed(2));
            $('#cost').val( parseFloat($('#cost').val()-prezzo).toFixed(2));
        }

    </script>
@endsection