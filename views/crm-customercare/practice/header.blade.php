<div class="row bg-title hidden-xs hidden-sm">
	<div class="col-12">
		<h4 class="pull-left page-title">Pratiche</h4>
		<a type="button" href="{{path_for('admin.crm.customercare.practice.add')}}" class="pull-left ml-5 btn btn-info btn-sm"><i class="fa fa-plus-square"></i> Aggiungi Nuova pratica</a>
		<a type="button" href="{{path_for('admin.crm.customercare.practice.list')}}"	class="pull-left ml-3 btn btn-default btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vai alla lista delle pratiche</a>
        <a type="button" href="{{path_for('admin.crm.customercare.practice.listarchived')}}"	class="ml-3 mb-3 btn btn-warning btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vedi pratiche chiuse</a>
        <a type="button" href="{{path_for('admin.crm.customer.add')}}"	class="ml-3 mb-3 btn btn-success btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Aggiungi cliente</a>
	</div>
</div>

<div class="row bg-title hidden-md hidden-lg hidden-xl">
    <div class="col-12 mb-3 text-center ">
        <h4 class="page-title">Pratiche</h4>
    </div>
    <div class="col-12 text-center">
        <a type="button" href="{{path_for('admin.crm.customercare.practice.add')}}" class="mb-3 btn btn-info btn-sm"><i class="fa fa-plus-square"></i> Aggiungi Nuova pratica</a>
        <a type="button" href="{{path_for('admin.crm.customercare.practice.list')}}"	class="ml-3 mb-3 btn btn-default btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vai alla lista delle pratiche</a>
        <a type="button" href="{{path_for('admin.crm.customercare.practice.listarchived')}}"	class="ml-3 mb-3 btn btn-warning btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Vedi pratiche chiuse</a>
        <a type="button" href="{{path_for('admin.crm.customer.add')}}"	class="ml-3 mb-3 btn btn-success btn-sm" style="color: #444; margin-left :1px"><i class="fa fa-list-alt"></i> Aggiungi cliente</a>
    </div>
</div>