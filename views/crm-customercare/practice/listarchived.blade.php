@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

        @include('crm-customercare.practice.header')

        <!-- START WIDGETS -->
            <div class="row">
                <div class="col-sm-12">
                    <h2>Pratiche Archiviate</h2>
                    <div class="white-box">
                        <div class="table-responsive">
                            <table id="tabella" class="table m-t-30 table-hover contact-list">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="selectallrow"></th>
                                    <th>ID</th>
                                    <th>Data</th>
                                    <th>Cliente</th>
                                    <th>Stato Pratica</th>
                                    <th>Tipologia</th>
                                    <th>Prodotto</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Data</th>
                                    <th>Cliente</th>
                                    <th>Stato Pratica</th>
                                    <th>Tipologia</th>
                                    <th>Prodotto</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>

                                <tbody>
                                @foreach ($table as $row)
                                    <tr>
                                        <td><input type="checkbox" class="selectrow" value="{{ $row->id }}"></td>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->created_at->format('d/m/Y') }}</td>
                                        <td>{{ $row->customer->name }} {{ $row->customer->surname }}</td>
                                        <td>{{ $row->getStatusLabel() }}</td>
                                        <td>{{ $row->typeOp->title}}</td>
                                        <td>{{ $row->typeOpProduct->title }}</td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="{{ path_for('admin.crm.customercare.practice.update', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-default">

                                                    <span class="fa fa-pencil"></span>
                                                </a>
                                                <a style="background: #999;" href="{{ path_for('admin.crm.customercare.practice.print', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-default">
                                                    <span class="fa fa-print"></span>
                                                </a>
                                                @if(!$row->express_repair)
                                                    <a data-path="{{ path_for('admin.crm.customercare.practice.sendsms', ['id' => $row->id]) }}"
                                                       class="btn btn-sm @if(!$row->sms_sended) btn-danger @else btn-success @endif sendSms" @if(!$row->sms_sended) style="background: #ff0000;" @endif>
                                                        <span class="fa fa-commenting-o"></span>
                                                    </a>
                                                @else
                                                    <a data-path="#"
                                                       class="btn btn-sm btn-info">
                                                        E
                                                    </a>
                                                @endif
                                                <a data-path="{{ path_for('admin.crm.customercare.practice.delete', ['id' => $row->id]) }}"
                                                   class="btn btn-sm btn-danger delete-item">
                                                    <span class="fa fa-trash-o"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5><strong>Azioni di gruppo</strong></h5>
                    <a id="deleteAll"
                       data-path="{{ path_for('admin.crm.customercare.practice.deletegroup', ['code_post_type' => $code_post_type]) }}"
                       class="btn btn-sm btn-danger delete-item"><span class='fa fa-trash-o'></span> Cancella</a>
                </div>
            </div>
            <!-- END WIDGETS -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/functions_custom.js')}}"></script>
    <script>
        $(document).ready(function () {

            var table = $('#tabella').DataTable({
                responsive: true,
                displayLength: 25,
                "order": 0,
                {{--
                language: {
                "url": "{{assetsBack}}lang/datatable/datatable-{% trans %}language{% endtrans %}.json"
                },
                --}}
                columns: [
                    {"searchable": false, "sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},
                    {"searchable": false, "sortable": false}
                ]
            });

//la trovi in function_custom.js
            tabellaInit(table);
            $('#tabella thead th').each(function () {
                if ($(this).index() !== 0 && !$(this).hasClass('no-filter')) {
                    var title = $('#tabella thead th').eq($(this).index()).text();
                    $(this).html('<input class="form-control" style="width:100%;" type="text" placeholder="' + title + '" />');
                } else {
                    $(this).html('');
                }
            });
            $('#tabella thead input').on('keyup change', function () {
                var indice = $(this).parent().index('#tabella thead th');
                table.column(indice).search(this.value).draw();
            });

        });
    </script>
@endsection