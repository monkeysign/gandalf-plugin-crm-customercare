<li>
    <a href="#" class="waves-effect"><i
                class="zmdi zmdi-mood zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Assistenza Clienti <span
                    class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <l1 class="text-center"><hr><h5><strong>Modulo Assistenza</strong></h5></l1>
        <li>
            <a href="{{path_for('admin.crm.customercare.practice.list')}}" class="waves-effect"><i
                        class="zmdi zmdi-assignment zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Pratiche <span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.crm.customercare.practice.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.crm.customercare.practice.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
        <l1 class="text-center"><hr><h5><strong>Impostazioni</strong></h5></l1>
        <li>
            <a href="{{ path_for('admin.crm.customercare.typeop.base') }}" class="waves-effect"><i
                        class="zmdi zmdi-apps zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Tipologie Pratica  <span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.crm.customercare.typeop.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.crm.customercare.typeop.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
        <li>
            <a href="{{ path_for('admin.crm.customercare.typeopproduct.list') }}" class="waves-effect"><i
                        class="zmdi zmdi-shape zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Prodotti<span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.crm.customercare.typeopproduct.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.crm.customercare.typeopproduct.add')}}">Aggiungi Nuova </a></li>

            </ul>
        </li>
        <li>
            <a href="{{ path_for('admin.crm.customercare.operation.list') }}" class="waves-effect"><i
                        class="zmdi zmdi-money zmdi-hc-fw fa-fw"></i> <span class="hide-menu"> Interventi Prod.<span
                            class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li><a href="{{path_for('admin.crm.customercare.operation.list')}}">Vai alla lista </a></li>
                <li><a href="{{path_for('admin.crm.customercare.operation.add')}}">Aggiungi Nuova </a></li>
            </ul>
        </li>
    </ul>
</li>