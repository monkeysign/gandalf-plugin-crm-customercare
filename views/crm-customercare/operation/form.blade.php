@extends('layout')

@section('content')

    <!-- PAGE CONTENT WRAPPER -->
    <div id="page-wrapper">
        <div class="container-fluid">

            @include('crm-customercare.operation.header')

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h2>
                            @if($record->id)
                                <strong>{{$record->title}}</strong> <span class="badge badge-info">Intervento</span>
                            @else
                                Nuovo Intervento
                            @endif
                        </h2>
                        <hr/>
                        <form id="form-save" action="{{ path_for('admin.crm.customercare.operation.save') }}" method="POST"
                              enctype="multipart/form-data">
                            <input id="item_id_hidden" type="hidden"
                                   value='@if($record->id){{ $record->id }}@else{{0}}@endif' name='item[id]'>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Titolo *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <input value="{{$record->title}}" required
                                                       data-toggle="validator" type="text" name="item[title]"
                                                       id="name" class="form-control"
                                                       placeholder="Titolo">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Prodotto *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-user"></i>
                                                </div>
                                                <select name="item[id_typeopproduct]"
                                                        data-placeholder="Seleziona il prodotto"
                                                        class="select2 select-choose form-control">
                                                    @foreach($allProduct as $prod)
                                                        <option @if($prod->id==$record->id_typeopproduct){{"selected"}}@endif value="{{$prod->id}}">{{$prod->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label title="Campo obbligatorio"
                                                   class="control-label">Prezzo *</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="ti-money"></i>
                                                </div>
                                                <input value="{{$record->price}}" required
                                                       data-toggle="validator" type="text" name="item[price]"
                                                       id="name" class="form-control"
                                                       placeholder="Prezzo">
                                            </div>
                                            <span class="help-block with-errors"> </span>
                                        </div>
                                    </div>

                                </div>

                                {{ hooks()->do_action(CRM_ADMIN_CUSTOMERCARE_OPERATION_FORM, $record) }}

                            </div>
                            <!--/row-->

                            <!--/span-->
                            <div class="form-actions">
                                <hr style="margin-top: 0px">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salva
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="{{asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('assets/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/form.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('assets/plugins/bower_components/blockUI/jquery.blockUI.js')}}"></script>

    <!-- Select Plugin JavaScript -->
    <script src="{{asset('assets/plugins/bower_components/custom-select/custom-select.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"
            type="text/javascript"></script>

    <script>
        // Select Choosen
        jQuery(document).ready(function () {
            $(".select-choose").select2();
        });
    </script>
@endsection